

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404</title>
    <style>
        .container{
            position: relative;
            height: 700px;
            width: 100%;
        }
        .error-404{
            border : 2px solid #ebccd1;
            color: #a94442;
            background-color: #f2dede;
            padding : 10px;
            font-size: 50px;
            position: absolute;
            margin: 0;
            left: 50%;
            top:50%;
            transform: translate(-50%,-50%);
            
           
    
        }
    
    </style>
</head>
<body>
    <div class="container">
         <p class="error-404"> page not found error . <a href="<?= WEB_ROOT.'/public/index.php?url=user/login' ?>">login</a></p>
    </div>
     
</body>
</html>
