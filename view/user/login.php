<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=WEB_ROOT."public/css/style.css" ?>">
    
    <title>login</title>
</head>
<body class='login'>
   <div id="report" class="report-container">
                <?php 
                       if (isset($error)){
		        
		        	   foreach ($error as $e){
		        	   	  
		        	   	    // echo '<div class="report danger-report"><h5>'.$e.'</h5></div>';
		        	   	     
		        	   }
		        	
		        }?>
		      
                   
	</div> 
    <div class="container">
           
            <form class="form login-form" method="post" action="<?=WEB_ROOT."public/index.php?url=user/login" ?>" >
    
                    <label class="label" for="1">username or email :</label>
                    <input required class="input" type="text" id="1" name="username">
           
                    <label class="label" for="2">password</label>
                    <input required class="input" type="password" id="2" name="password">
                    <label class="label" for="2" >
                        <div style="position:relative">
                            <img style="margin:20px;" src="<?=WEB_ROOT."public/index.php?url=captcha" ?>" alt=""> 
                            <a style="display:block ; margin:7px;width:100px;position:absolute;top:25%;right:40%" href="<?=WEB_ROOT."public/index.php?url=user/login" ?>">Actuialiser</a>
                        </div>
                        
                    </label><br>
                    <label class="label" for="3">captcha</label>
                    <input required class="input" type="text" id="3" name="captcha">

                    <input class="btn btn-submit" type="submit" name="login" id="loginButton">
                    <input class="btn btn-reset" type="reset" name="reset" id="">
           
            </form>

    </div>
    <script src="<?=WEB_ROOT."public/js/ajax.js" ?>" type="text/javascript"></script>
    <script src="<?=WEB_ROOT."public/js/login.js" ?>" type="text/javascript"></script>
</body>
</html>