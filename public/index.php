<?php
session_start();

use controller\userController;
use tools\viewModel;
use tools\captcha;

define('FILE_ROOT', str_replace('public/index.php','',$_SERVER["SCRIPT_FILENAME"]));
define('WEB_ROOT',str_replace('public/index.php','',$_SERVER["SCRIPT_NAME"]));

if(!isset($_GET["url"])){
	$url="user/action";
}else{
	$url=$_GET["url"];
}


$m=explode("/",$url);

$controller =isset($m[0]) && !empty($m[0]) ? $m[0]:"user";
$action = isset($m[1]) && !empty($m[1]) ? $m[1].'Action':"loginAction";

require FILE_ROOT.'src/tools/captcha.php';
require FILE_ROOT.'src/tools/Session.php';
require FILE_ROOT.'src/tools/viewModel.php';

if($m[0]=="captcha"){
	
	 $cap=new captcha(FILE_ROOT.'public/fonts/arial.ttf');
	 $cap->setCaptcha();
	 die();
	
}
if(!file_exists(FILE_ROOT.'src/controller/'.$controller."Controller.php")){
	(new viewModel("error/404"))->Rederect(404) ;
	die();
}

require FILE_ROOT."src/controller/".$controller."Controller.php";

$c=new userController();

	if(method_exists("controller\\".$controller."Controller", $action)){
		
		require FILE_ROOT."src/model/".$controller.".php";
		require FILE_ROOT."src/model/".$controller."Model.php";
		
		unset($m[0]);
		unset($m[1]);
		$controller="controller\\".$controller."Controller";
		$c=new $controller;
		
		call_user_func_array([$c,$action], $m);
        
	}else{
		
		http_response_code(404);
		//header("location:".WEB_ROOT."view/error/404.php");
		(new viewModel("error/404"))->Rederect(404) ;
		
	}






?> 