<?php

namespace controller ;

use model\user;
use model\userModel;
use tools;
use tools\viewModel;
use tools\Session;

class userController{
	
	public function loginAction(){
		
		//Session::deconnexion();die("y");
		sleep(2);
		
		if(Session::getSession("email") && Session::getSession("password")){
			(new viewModel("user/profile"))->Rederect(200);
		}
		
		if(isset($_POST["username"])){

			$error=[];
			
			$email=isset($_POST["email"])?$_POST["email"]:NULL;
			$captcha=isset($_POST["captcha"])?$_POST["captcha"]:NULL;
			$password=isset($_POST["password"])?$_POST["password"]:NULL;
			$username=isset($_POST["username"])?$_POST["username"]:NULL;
			
			/**
			 *   var validation
			 */
			
			if($email==null or filter_var($email,FILTER_VALIDATE_EMAIL)==false){
					
				if($username==null){
					array_push($error, "invalid email/username or password ");
				}
					
			}
			
			if($password==null){
					
				if($error==[])
					array_push($error, "invalid email/username or password ");
						
			}
			
			if($captcha==null or Session::getSession("captcha")!=$captcha){
				array_push($error, "invalid captcha ");
			}
			
			if($error==[]){
					
				$user=new user($username, $email, $password);
				$model=new userModel();
				$res=$model->userExists($user);
				if($res==true){
				    Session::setSession("email",$email);
				    Session::setSession("password",password_hash($password,PASSWORD_DEFAULT));
					echo json_encode(["to"=>"index.php?url=user/profile"]);
						
				}else{
						
					array_push($error, "invalid email/username or password ");
					http_response_code("400");
					echo json_encode(["result"=>$error]);
					die();
				}
			
			}else {
				http_response_code("400");
				echo json_encode(["result"=>$error]);
				die();
			}
			
		}else{
			
			(new viewModel("user/login"))->Rederect(200);
			
		}
		
		
	
	}
	
	public function logoutAction(){
		
		tools\Session::deconnexion();
		(new viewModel("user/login"))->Rederect(200);
		
	}
	
	public function profileAction(){
		(new viewModel("user/profile"))->Rederect(200);
	}
	
}