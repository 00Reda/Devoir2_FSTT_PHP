<?php
namespace tools;

   class captcha {
   	 
   	   private $font;
   	   private $h;
   	   private $l;
   	  
   	
   	public function __construct($font,$h=70,$l=250){
   		
   		 $this->font=$font;
   		 $this->h=$h;
   		 $this->l=$l;
   		
   	}
   	
   
       
   	
   	public function setCaptcha(){
   		
   		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
   		$string = '';
   		
   		$max = strlen($characters) - 1;
   		
   		for ($i = 0; $i < 8; $i++) {
   			$string .= $characters[mt_rand(0, $max)];
   		}
   		
   		Session::setSession("captcha",$string);
   		
   		header("Content-type: image/png");
   		 
   		$img      =imagecreate($this->l,$this->h);
   		
   		$grey	  =imagecolorallocate($img,110,128,128);
   		
   		$black    =imagecolorallocate($img, 0,0,0);
   		 
   		imagecolortransparent($img,$black);
   		 
   		
   		
   		
   		imagettftext($img, 32,0,20,60,$black,$this->font,Session::getSession("captcha")."");
   		 
   		imagepng($img);
   		 
   		imagedestroy($img);
   		
   		
   	}
   	
   
   }