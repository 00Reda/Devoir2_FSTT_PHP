<?php

namespace model;

class userModel{
	
	private $db=[];
	
	public function __construct(){
														
		$u1=new user("rida","reda.htiti20@gmail.com",'$2y$10$UQYtdr2c0eLhgoErk5ftVu2gyFx1dV4iOmDIxMx6WxMlKccbS2FBK');
		$u2=new user("mohammed","mohammed@gmail.com",'$2y$10$UQYtdr2c0eLhgoErk5ftVu2gyFx1dV4iOmDIxMx6WxMlKccbS2FBK');
		$this->db[0]=$u1;
		$this->db[1]=$u2;
				
	}
	
	public function userExists($user){
		
		$res=false;
		foreach ($this->db as $elm){
			
			if( ($user->getUsername()==$elm->getUsername() or $user->getEmail()==$elm->getEmail() or $user->getUsername()==$elm->getEmail()) and password_verify($user->getPassword(),$elm->getPassword())){
			    $res= true;break;
			}
			
		}
		
		return $res;
		
	}
	
	
}